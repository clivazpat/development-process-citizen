= Test Levels Overview
:xrefstyle: short

== Uniface configuration
It is important to understand the different possibilities of Uniface configuration. In a typical configuration for Citizen, the Uniface runtime environment is made available on a server, accessible via a network drive, but runs as a thick client directly on the workstation. It is structured as follows:

* **Uniface folder**
** List of Uniface environment executables and files
* **Configuration files**
** idf97x.asn - Configuration file for running the Uniface deployment environment
*** Uniface repository (application for development)
Relational database containing Uniface development object definitions
*** Database connection (Citizen)
*** List of UARs resources
** is97x.asn - Configuration file for running Citizen.
*** Database connection (Citizen)
*** List of UARs resources to be called
* **Executable shortcuts**
** Enable to execute the desired environment via the configuration files in parameter

The runtime objects can then be called by the configuration files from 2 locations :

* **Resources (Files)**
** 'Raw' compiled files, at their direct path
** Used only in Development
* **Resources (UARs)**
** Uniface Enforcement Records
** An ordered list of compiled files
** Must be generated manually
** Installed in customer environments

== Tests Environments
Various environments exist and are currently set up for the Citizen team. A Citizen environment is simply defined in its configuration file, which is called by the executable shortcut that will be launched. It is thus possible to have a multitude of environments per physical machine.

.Citizen current Tests Environments
[#CitizenEnvironments]
image::CitizenEnvironments.drawio.svg[link=_images/CitizenEnvironments.drawio.svg, window="_blank"]

There are usually 2 types of configuration at the moment.

=== Development Environment

Executed from the local workstation, DBMS connection and file server configured directly on a network drive. Configuration for 'Development' (objects called from source file path and development database), 'UAR' (same but UAR archives called instead of objects), 'Test Dev' (objects called from source file path, representative client database) and 'Regression' (objects called from a different source file path, previous object versions can also be imported, development database).

.Development Environment Configuration Scheme
[#DevEnvironment]
image::DevEnvironment.png[link=_images/DevEnvironment.png, window="_blank"]

=== PreProd/Customer Environment
Connection to a separate server using RDP protocol and which has its own configuration (specific Uniface version on the machine, archives called from the machine, database available from the machine). This environment provides a complete client representative environment and offers the possibility to perform integration tests with external interfaces and other T2i applications. This configuration is also often used internally to create temporary environments when transferring data to municipalities. In production, it is the configuration that is most often preferred by all customers.

.PreProd and Customer environment configuration scheme
[#PreProdEnvironment]
image::PreProdEnvironment.png[link=_images/PreProdEnvironment.png, window="_blank"]

== Actual State
The following table (<<Overview>>) shows an overview of the actual tests of different test levels for Citizen (August 2020).

.Test Levels overview
[#Overview]
image::TestLevelsOverview.drawio.svg[link=_images/TestLevelsOverview.drawio.svg, window="_blank"]

== T2i Overview in the future

This section presents a global Test Levels overview that is foreseen for all products in the T2i test strategy.

.Test Levels overview in the future (T2i)
[cols="3,2,2,1,2,1,2" options="header"]
|===


|Description
|Documentation
|Test Environment
|Who
|When
|Auto-matic
|Scope

|||||||
h|UNIT TESTS||||||

a|**Tests of methods:**

*  for each method that
contains business logic
(no getter or setter!)
*  at least one positive
and negative test
(e.g. getUserName()
* external frameworks
are mocked
|GitLab
a|Local machine

Build server

* database H2 in memory

|SCRUM Team
|Each build
|YES
|Backend

Method

a|**REST API tests of the product:**

* each API must be tested
|GitLab

Suggestion: Swagger
a|Local machine

Build server

* database H2 in memory
|SCRUM Team
|Each build
|YES
|Backend

Object

|||||||
h|INTEGRATION TESTS||||||

a|**Framework & Product Integration**

* each integration of a framework or another product has to be tested
|tbd
|DEV: Integration
|SCRUM Team
|After each **story** or **hotfix branch** merge
|YES
|Backend

Cross-Product-Objects

|||||||
h|SYSTEM TESTS||||||

a|**User Story Verification:**

* after implementing user story these tests are executed
* created during the functional analysis and detailed during this test level
|SpiraTest
|DEV: System Master
|SCRUM Team
|After each user story that has added functionality
|NO
|Frontend/ Backend

User Story

a|**Sprint Verification:**

* set of regression tests
* user stories verification (Tester)
|SpiraTest
|DEV: System Master
|SCRUM Team, Tester
|At the end of the sprint
|YES and NO
|Frontend/ Backend

User Story/ Cross Product

a|**Release Verification:**

* new functionalities
* regression tests
|SpiraTest
|DEV: System Release
|SCRUM Team, Tester
|For each Beta, RC and Release
|YES and NO
|Frontend

Cross-Product


|||||||
h|ACCEPTANCE TESTS||||||

a|**Business Validation:**

* business use cases are tested
|SpiraTest
|BU: Test
|BU
|For each Release
|NO
|Frontend

Cross-Product


|||||||
h|STAGING SAAS||||||
a|**SaaS Verification**

*  Set of regression tests
|tbd
|tbd
|tbd
|tbd
|tbd
|Frontend

Cross-Product

|===
