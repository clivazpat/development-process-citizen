= Acceptance Tests
:xrefstyle: short

|===
|**Definition:** +
The purpose of acceptance testing is to validate that specific business requirements are met.
|===

The following table shows the test activities of the test level Acceptance Tests.
[%header, cols=3;^1, width=50%]
|===

|Activity
|Yes

||

|**Functional properties**
|&#10060;

||

|**Quality properties** +
|&#9989;

|Functional Suitability +
Performance efficiency +
Compatibility +
Usability +
Reliability +
Security +
Maintainability +
Portability
|&#9989; +
&#10060; +
&#10060; +
&#9989; +
&#10060; +
&#10060; +
&#10060; +
&#10060; +
|===


For Citizen we have one kind of Acceptance Test:

* **End2End Acceptance Tests in a specific context only (PM)**
