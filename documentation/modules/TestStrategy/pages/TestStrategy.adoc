= Test Strategy
:xrefstyle: short

== V-Model

As defined by the global T2i Test Strategy, we use the V-model to define the test levels (<<TestStrategy>>). On the left are the development phases and on the right the associated test levels. The bottom consists of the coding phase.

.V-Model
[#TestStrategy]
image::VModel.drawio.svg[link=_images/VModel.drawio.svg, window="_blank"]

As we try to introduce an agile development process (SCRUM), all the phases are run through for each feature and their user stories. This starts with the requirements analysis of the Product Manager. This is the basis to create the functional analysis of the new features to implement. The Product Owner writes then this functional analysis that contains some basic technical elements. This activity as well as the definition of User Stories can be done with several interactions with Software Architects (Senior Developers). Afterwards the developer(s) concerned writes a precise technical analysis in order to respond to the functional requirements. It is important to evaluate the consequences of the new features to the system, technical and module design of the product. Then, the feature and its User Stories are coded.

Every development phase has a corresponding test level associated. Totally, there are four test levels:

•	xref:UnitTests.adoc[Unit Tests]
•	xref:IntegrationTests.adoc[Integration Tests]
•	xref:SystemTests.adoc[System Tests]
•	xref:AcceptanceTests.adoc[Acceptance Tests]

Unit and Integration tests are White Box tests, whereas System and Acceptance tests are Black Box tests (definition in the xref:TestStrategyGlossary.adoc[Glossary]).

Each test level has a different testing focus and can have 1 to n different test environments.

== Test automation pyramid
:xrefstyle: short

<<Pyramid>> shows the test automation pyramid. It is divided into three levels:

•	Unit tests
•	Integration tests
•	UI tests

If you climb the pyramid, the tests get slower and more expensive. Therefore, the amount of tests should decrease from the bottom to the top of the pyramid. This is represented by the width of the pyramid.

The product Citizen has been developed for 20 years and has a lot of technical obstacles for the implementation of automation. In the long term and as for all other T2i products, the goal is to greatly favour the automation of unit and integration tests and some UI tests. Although this element is to be taken into account, the automation part will not be explicitly described in this first version of the test strategy.

.Test Automation Pyramid
[#Pyramid]
image::TestAutomationPyramid.png[link=_images/TestAutomationPyramid.png, window="_blank"]


== Test scope
:xrefstyle: short

The test scope can be divided into two main properties:

•	**Functional properties:** +
They determine what the software is able to do. To test the functional properties means to verify if the functional requirements that are defined in the functional analysis, are fulfilled.
•	**Quality properties (non-functional):** +
They determine how well the software performs. To test the quality properties means to verify if the non-functional requirements that are defined in the functional analysis, are fulfilled.

The test scope is applicable on all test levels.

In the following table, the quality properties according to _ISO 25010_ are shown. This gives an overview what kind of non-functional properties exists.

.Quality properties (non functional)
[%header,cols=".^1,2",stripes=all]
|===
|Quality characteristic
|Quality criterion

.3+|**Functional suitability** +
degree to which a product or system provides functions that meet stated and implied needs when used under specified conditions
|**Functional completeness** +
degree to which the set of functions covers all the specified tasks and user objectives
|**Functional correctness** +
degree to which a product or system provides the correct results with the needed degree of precision
|**Functional appropriateness** +
degree to which the functions facilitate the accomplishment of specified tasks and objectives

.3+|**Performance efficiency** +
performance relative to the amount of resources used under stated conditions
|**Time behaviour** +
degree to which the response and processing times and throughput rates of a product or system, when performing its functions, meet requirements
|**Resource utilization** +
degree to which the amounts and types of resources used by a product or system, when performing its functions, meet requirements
|**Capacity** +
degree to which the maximum limits of a product or system parameter meet requirements

.2+|**Compatibility** +
degree to which a product, system or component can exchange information with other products, systems or components, and/or perform its required functions, while sharing the same hardware or software environment
|**Co-existence** +
degree to which a product can perform its required functions efficiently while sharing a common environment and resources with other products, without detrimental impact on any other product
|**Interoperability** +
degree to which two or more systems, products or components can exchange information and use the information that has been exchanged

.6+|**Usability** +
degree to which a product or system can be used by specified users to achieve specified goals with effectiveness, efficiency and satisfaction in a specified context of use
|**Appropriateness recognisability** +
degree to which users can recognize whether a product or system is appropriate for their needs
|**Learnability** +
degree to which a product or system can be used by specified users to achieve specified goals of learning to use the product or system with effectiveness, efficiency, freedom from risk and satisfaction in a specified context of use
|**Operability** +
degree to which a product or system has attributes that make it easy to operate and control
|**User error protection** +
degree to which a system protects users against making errors
|**User interface aesthetics** +
degree to which a user interface enables pleasing and satisfying interaction for the user
|**Accessibility** +
degree to which a product or system can be used by people with the widest range of characteristics and capabilities to achieve a specified goal in a specified context of use

.4+|**Reliability** +
degree to which a system, product or component performs specified functions under specified conditions for a specified period of time
|**Maturity** +
degree to which a system, product or component meets needs for reliability under normal operation
|**Availability** +
degree to which a system, product or component is operational and accessible when required for use
|**Fault tolerance** +
degree to which a system, product or component operates as intended despite the presence of hardware or software faults
|**Recoverability** +
degree to which, in the event of an interruption or a failure, a product or system can recover the data directly affected and re-establish the desired state of the system

.5+|**Security** +
degree to which a product or system protects information and data so that persons or other products or systems have the degree of data access appropriate to their types and levels of authorization
|**Confidentiality** +
degree to which a product or system ensures that data are accessible only to those authorized to have access
|**Integrity** +
degree to which a system, product or component prevents unauthorized access to, or modification of, computer programs or data
|**Non-repudiation** +
degree to which actions or events can be proven to have taken place, so that the events or actions cannot be repudiated later
|**Accountability** +
degree to which the actions of an entity can be traced uniquely to the entity
|**Authenticity** +
degree to which the identity of a subject or resource can be proved to be the one claimed

.5+|**Maintainability** +
degree of effectiveness and efficiency with which a product or system can be modified by the intended maintainers
|**Modularity** +
degree to which a system or computer program is composed of discrete components such that a change to one component has minimal impact on other components
|**Reusability** +
degree to which an asset can be used in more than one system, or in building other assets
|**Analysability** +
degree of effectiveness and efficiency with which it is possible to assess the impact on a product or system of an intended change to one or more of its parts, or to diagnose a product for deficiencies or causes of failures, or to identify parts to be modified
|**Modifiability** +
degree to which a product or system can be effectively and efficiently modified without introducing defects or degrading existing product quality
|**Testability** +
degree of effectiveness and efficiency with which test criteria can be established for a system, product or component and tests can be performed to determine whether those criteria have been met

.3+|**Portability** +
degree of effectiveness and efficiency with which a system, product or component can be transferred from one hardware, software or other operational or usage environment to another
|**Adaptability** +
degree to which a product or system can effectively and efficiently be adapted for different or evolving hardware, software or other operational or usage environments
|**Installability** +
degree of effectiveness and efficiency with which a product or system can be successfully installed and/or uninstalled in a specified environment
|**Replaceability** +
degree to which a product can replace another specified software product for the same purpose in the same environment
|===
